/**
 * img card
 * 
 * build by browserify
 * by tobeyouth@gmail.com
 */
var defaultConfig = {
	'tpl': 	'<a href="<%= link %>" class="img-card">' +
				'<div class="imgbox">' +
					'<img data-src="<%= src %>" src="<%= loading %>"/>'+
					'<div class="title"><%= title %></div>'+
				'</div>'+
				'<div class="content"><%= content %></div>'+
			'</a>',
	'afterShow': null
};

var ImgCard = function (config) {
	this.config = $.extend({},defaultConfig,config);
	this.loaded = false;
	this.init();
};
ImgCard.prototype = {
	"init": function () {
		
	},
	"loadImg": function () {

	}
}

exports.createImgCard = function (config) {
	return new ImgCard(config);
};
