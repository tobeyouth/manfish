/**
 * 滚动卡片
 *
 * by tobeyouth@gmail.com
 */
var defaultConfig = {
	'wrap': '#img-card-list-wrap',
	'tpl': 	'<div class="img-card-list" style="width:<%= listWidth %>px;left:<%= station %>px;">' +
				'<%for (var i = 0,len = data.length;i < len;i++) { %>' +
					'<a style="width:<%= cardWidth %>px;height:<%= cardHeight %>px;" href="<%= data[i].link %>" class="img-card">' +
						'<div class="imgbox">' +
							'<img src="<%= data[i].src %>" width="100%" />'+
							'<div class="title"><%= data[i].title %></div>'+
						'</div>'+
						'<div class="content"><%= data[i].content %></div>'+
					'</a>'+
				'<%}%>'+
			'</div>',
	'data': [],
	'offset': 21,
	'beforeSlide': null,
	'afterSlide': null
};
var widthRate = 0.72;	// 图片宽度和屏幕宽度的比
var heightRate = 0.79;


var ImgCardList = function (config) {
	this.config = $.extend({},defaultConfig,config);
	this.wrap = $(this.config.wrap);
	this.index = 0;
	this.init();
};
ImgCardList.prototype = {
	'constructor': ImgCardList,
	'init': function () {
		var list = this;
		var wrapWidth = list.wrap.width() || window.screen.availWidth;
		var cardWidth = wrapWidth * widthRate;
		var cardHeight = cardWidth / heightRate;
		var data = {
			"station": Math.round((wrapWidth / 2) - (cardWidth / 2) + 10),
			"listWidth": Math.round((cardWidth + 20) * list.config.data.length),
			"cardWidth": Math.round(cardWidth),
			"cardHeight": Math.round(cardHeight + 30),
			// "listHeight": Math.round(cardHeight + 22),
			"data": list.config.data
		};
		var render = template.compile(list.config.tpl);
		var html = render(data);
		list.dom = $(html);
		list.wrap.append(list.dom);
		list.offset = {
			"station": data.station,
			"listWidth": data.listWidth,
			"cardWidth": data.cardWidth,
			"cardHeight": data.cardHeight,
			"listHeight": Math.round(list.dom.height())
		};
		list.wrap.css({'height':list.offset.listHeight+'px'});
		list.bindEvent();
		return list;
	},
	'bindEvent': function () {
		var list = this;

		list.dom.bind('swipeLeft',function (e) {
			list.scrollLeft();
		});
		
		list.dom.bind('swipeRight',function (e) {
			list.scrollRight();
		});	

		return list;
	},
	'scrollLeft': function () {
		var list = this;
		var station = parseInt(list.dom.css('left')) || 0;
		var step = Math.round(list.offset.cardWidth + list.config.offset);
		
		if (list.index >= (list.config.data.length - 1)) {
			return list;
		};

		list.index += 1;
		list.scroll(station - step);

		return list;
	},
	'scrollRight': function () {
		var list = this;
		var station = parseInt(list.dom.css('left')) || 0;;
		var step = Math.round(list.offset.cardWidth + list.config.offset);

		if (list.index <= 0) {
			return list;
		};

		list.index -= 1;
		list.scroll(station + step);

		return list;
	},
	'scroll': function (dist) {
		var list = this;
		list.dom.css({
			"left": dist + 'px'
		});
		list.dom.children().each(function (index) {
			if (index != list.index) {
				$(this).removeClass('cur');
			} else {
				$(this).addClass('cur');
			}
		});

		return list;
	}
};

createImgCardList = function (config) {
	return new ImgCardList(config);
};
